package com.functionalinterface;

/*
class Demo implements Interf4{

	@Override
	public int getSquare(int x) {
		return x*x;
	}
	
}

class Test{
	public static void main(String[] args) {
		Interf4 i = new Demo();
		System.out.println(i.getSquare(3));
		System.out.println(i.getSquare(4));
	}
}
*/

public class FunctionalInterfaceExample4 {

	public static void main(String[] args) {
		Interf4 i = x -> x*x;
		System.out.println(i.getSquare(3));
		System.out.println(i.getSquare(4));

	}

}
