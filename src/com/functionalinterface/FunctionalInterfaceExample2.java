package com.functionalinterface;

/*class Demo implements Interf2 {

	@Override
	public void add(int a, int b) {
		System.out.println(a + b);
	}
}

class Test {
	public static void main(String[] args) {
		Interf2 i = new Demo();
		i.add(10, 30);
		i.add(5, 7);
	}
}
*/

public class FunctionalInterfaceExample2 {
	public static void main(String[] args) {
		Interf2 i = (a, b) -> System.out.println(a + b);
		i.add(10, 20);
		i.add(5, 7);
	}
}
