package com.functionalinterface;

/*
class Demo implements Interf3 {

	public int getLength(String s) {
		return s.length();
	}

}

class Test{
	public static void main(String[] args) {
		Interf3 i = new Demo();
		System.out.println(i.getLength("hello"));
		System.out.println(i.getLength("hello hello"));
	}
}
*/


public class FunctionalInterfaceExample3 {

	public static void main(String[] args) {
		Interf3 i = s -> s.length();
		System.out.println(i.getLength("hello"));
		System.out.println(i.getLength("hello hello"));
	
	}
}
