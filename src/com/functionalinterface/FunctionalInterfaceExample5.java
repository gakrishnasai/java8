package com.functionalinterface;

/*
class myRunnable implements Runnable {

	@Override
	public void run() {
		for (int i = 0; i < 10; i++)
			System.out.println("Child Thread");
	}
}
*/

public class FunctionalInterfaceExample5 {

	public static void main(String[] args) {

		// Without using lambda expression
//		Runnable r = new myRunnable();

		// Using lambda expression
		Runnable r = () -> {
			for (int i = 0; i < 10; i++)
				System.out.println("Child Thread");
		};

		Thread t = new Thread(r);
		t.start();
		for (int i = 0; i < 10; i++)
			System.out.println("Parent Thread");
	}
}
