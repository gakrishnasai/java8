package com.functionalinterface.predicate;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.function.Predicate;

public class PredicateBasics {

	public static void main(String[] args) {
		
		Predicate<Integer> greaterThan = i -> i>10;
		System.out.println("100 > 10 " + greaterThan.test(100));
		System.out.println("5 > 10 " + greaterThan.test(5));
		
		Predicate<String> minLength = i -> i.length() > 3;
		System.out.println("abcdef " + minLength.test("abcedf"));
		System.out.println("abcd " + minLength.test("abcd"));
		
		List<Integer> al = new ArrayList<Integer>();
		Predicate<Collection> status = c -> c.isEmpty();
		System.out.println("Initial Collection check " + status.test(al));
		al.add(10);
		System.out.println("Later Collection check " + status.test(al));

	}

}
