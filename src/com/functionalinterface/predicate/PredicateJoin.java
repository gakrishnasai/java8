package com.functionalinterface.predicate;

import java.util.function.Predicate;

//negate, and, or
public class PredicateJoin {

	public static void bulbul(Predicate<Integer> p, int[] x) {
		for (int i : x)
			if (p.test(i))
				System.out.print(i + " ");
	}

	public static void main(String[] args) {
		int[] numbers = { 0, 5, 10, 15, 20, 25, 30 };

		Predicate<Integer> greaterThan10 = i -> i > 10;
		Predicate<Integer> ifEven = i -> i % 2 == 0;

		System.out.println("Numbers greater than 10 ");
		bulbul(greaterThan10, numbers);
		System.out.println("\nNumbers less than 10 ");
		bulbul(greaterThan10.negate(), numbers);
		System.out.println("\nEven numbers are");
		bulbul(ifEven, numbers);
		System.out.println("\nNumbers greater than 10 and even numbers ");
		bulbul(greaterThan10.and(ifEven), numbers);
		System.out.println("\nNumbers less than 10 or even numbers");
		bulbul(greaterThan10.negate().or(ifEven), numbers);
		
		
	}

}
