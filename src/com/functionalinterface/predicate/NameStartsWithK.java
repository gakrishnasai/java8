package com.functionalinterface.predicate;

import java.util.function.Predicate;

public class NameStartsWithK {

	public static void main(String[] args) {
		String[] names = {"kajal", "sunny", "kareena", "cricket", "koolaid", "fuckoff"};
		Predicate<String> startsWithK = s -> s.charAt(0) == 'k';
		
		for (String str : names) {
			if(startsWithK.test(str)) {
				System.out.println(str);
			}
		}
	}

}
