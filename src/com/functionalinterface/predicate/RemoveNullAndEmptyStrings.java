package com.functionalinterface.predicate;

import java.util.function.Predicate;

public class RemoveNullAndEmptyStrings {

	public static void main(String[] args) {
		String[] names = { "hello", "", null, "", "bolo", "lalu" };
		Predicate<String> removeNullEmpty = s -> s != null && s.length() != 0;

		for (String str : names) {
			if (removeNullEmpty.test(str)) {
				System.out.print(str + " ");
//				System.out.print("--");
			}
		}

	}

}
