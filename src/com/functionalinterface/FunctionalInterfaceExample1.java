package com.functionalinterface;

/*class Demo implements Interf1 {

@Override
public void m() {
	System.out.println("m() method implementation");
}
}

class Test{
public static void main(String[] args) {
	Interf1 i = new Demo();
	i.m();
}
}*/

public class FunctionalInterfaceExample1 {
	public static void main(String[] args) {
		Interf1 i = () -> System.out.println("m() method implementation");
		i.m();
	}
}

