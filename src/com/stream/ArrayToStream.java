package com.stream;

import java.util.stream.Stream;

public class ArrayToStream {

	public static void main(String[] args) {
		String[] names = {"ram", "shyam", "gita", "sita"};
		int[] numbers = {1, 5, 2, 4, 3};
		
		Stream group = Stream.of(names);
		group.forEach(System.out :: println);
		
		Stream set = Stream.of(numbers);
		set.forEach(p -> System.out.println(p));
			
		} 		

}
