package com.stream;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class EvenOdd {

	public static void main(String[] args) {
		ArrayList<Integer> al = new ArrayList<Integer>();
		al.add(0);
		al.add(10);
		al.add(20);
		al.add(5);
		al.add(15);
		al.add(25);
		System.out.println("Main Array List");
		System.out.println(al);
		
		EvenWithoutStreams(al);
		EvenWithStreams(al);
		CreateNewListFromGivenList(al);
		CountExample(al);
		SortExample(al);
		ComparatorExample(al);
		Min(al);
		Max(al);
		ForEachORMethodReference(al);
		ToArray(al);
		GroupOfValues();
		ArrayStreams();

	}
	
	public static void EvenWithoutStreams(ArrayList<Integer> al) {
		List<Integer> l = new ArrayList<Integer>();
		for (Integer i : al) {
			if (i % 2 == 0) {
				l.add(i);
			}
		}
		System.out.println("\nFind Even numbers without using Streams");
		System.out.println(l);
	}
	
	//Filter Example
	private static void EvenWithStreams(ArrayList<Integer> al) {
		System.out.println("\nFind Even numbers using Streams");
		System.out.println(al.stream().filter(i -> i%2 == 0).collect(Collectors.toList()));
	}

	//Map Example
	public static void CreateNewListFromGivenList(ArrayList<Integer> al){
		System.out.println("\nCreating new list from a given list following some condition");
		System.out.println(al.stream().map(a -> a*2).collect(Collectors.toList()));
	}
	
	//Count Example -- finding number of odd numbers
	public static void CountExample(ArrayList<Integer> al) {
		System.out.println("\nCount Example -- finding number of odd numbers");
		System.out.println(al.stream().filter(i -> i%2 != 0).count());
	}
	
	//Sort Example
	private static void SortExample(ArrayList<Integer> al) {
		System.out.println("\nSort Example -- to align in ascending order");
		System.out.println(al.stream().sorted().collect(Collectors.toList()));
	}
	
	//Comparator Example
	private static void ComparatorExample(ArrayList<Integer> al) {
		System.out.println("\nComparator Example -- to align in descending order");
		System.out.println(al.stream().sorted((i1,i2)->i2.compareTo(i1)).collect(Collectors.toList()));
	}
	
	//max() example
	private static void Min(ArrayList<Integer> al) {
		System.out.println("\nMin function using comparator ");
		System.out.println(al.stream().max((i1,i2) -> i1.compareTo(i2)).get());
	}
	
	//min() example
	private static void Max(ArrayList<Integer> al) {
		System.out.println("\nMax function using comparator ");
		System.out.println(al.stream().min((i1,i2) -> i1.compareTo(i2)).get());
	}
	
	//forEach() and Method Reference example
	private static void ForEachORMethodReference(ArrayList<Integer> al) {
		System.out.println("\nforEach function example");
		al.stream().forEach(i -> System.out.print(i + " "));
		al.stream().forEach(System.out :: println);
	}
	
	//toArray example
	private static void ToArray(ArrayList<Integer> al) {
		System.out.println("\ntoArray example");
		Integer[] arr = al.stream().toArray(Integer[] :: new);
		for(Integer i : arr)
			System.out.print(i + " ");
	}
	
	//Streams works for any group of values
	private static void GroupOfValues() {
		System.out.println("\nStreams work for any group of values");
		Stream<Integer> s = Stream.of(9,99,999,9999);
		s.forEach(System.out :: print);
	}
	
	//Use Streams for Arrays
	private static void ArrayStreams() {
		Double[] d = {10.0, 11.1, 12.2, 13.3, 14.4}; 
		System.out.println("\nUse Streams for Arrays");
		Stream<Double> arrStream = Stream.of(d);
		arrStream.forEach(System.out :: print);
	}
}
