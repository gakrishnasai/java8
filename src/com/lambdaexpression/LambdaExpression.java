package com.lambdaexpression;

interface Drawable {
	public void draw();
}

interface Sayable {
	public String say();
}

interface SingleParameter {
	public String singleParameter(String name);
}

public class LambdaExpression {
	public static void main(String[] args) {
		int width = 10;
		
		//Without Lambda Expression
		Drawable d1 = new Drawable() {
			@Override
			public void draw() {
				System.out.println("d1 " + width);
			}
		};		
		d1.draw();
		
		//With lambda expression
		Drawable d2 = () -> System.out.println("d2 " + width);
		d2.draw();
		
		//Lambda Expression with no parameters
		Sayable s = () -> "I have nothing to say"; //return "I have nothing to say"
		System.out.println(s.say());
		
		//Lambda Expression with Single parameter
		SingleParameter sp = name -> {
			return "Hello " +name;
		};
		System.out.println(sp.singleParameter("Krishna"));
	}
}
